# Run on the HEC
#storage = Sys.getenv("global_scratch")
#t <- as.numeric(Sys.getenv('SGE_TASK_ID'))

t=1
K=2

N_folds <- 3
N_lambdas <- 4
lambdas <- c(exp(seq(0, 15, length = N_lambdas))) # Define the lambdas to choose from
lambda <- lambdas[rep(1:N_lambdas, times=N_folds, each=1)[t]]
fold <- rep(1:N_folds, times=1, each=N_lambdas)[t]
#K <- floor((t-1)/(N_folds*N_lambdas))+2

set.seed(100)

library(here)
library(Metrics)
library(mvtnorm)
library(glmnet)
library(MASS)
library(MCMCglmm)
library(doParallel)
library(clusterGeneration)
library(parallel)
library(optimParallel)
library(Matrix)
library(glasso)


source(here('ljcr_on_sim_data.R'))

ljcr_cv <- function(M = 500,
                    n_i = 10,
                    K = 2, 
                    p = 20,
                    N_folds = 3,
                    N_lambdas = 2,
                    N_EM_iteration = 40){
  
  N_folds = N_folds ## Number CrossValidation folds
  N_lambdas = N_lambdas ## Length of lambda vector to choose from in CrossValidation
  
  M = M ## Number of patients
  K = K ## Number of groups 
  N_EM_iteration <- N_EM_iteration # Number EM iterations
  n_i = n_i ## Observations per patient
  N = n_i*M ## Total number of data points
  p = p ## Number of covariates
  q = 2 ## Number of random effects
  y = list(); X = list()
  Z = list(); b = list()
  size = list()
  size = sapply(1:N_EM_iteration, function(i) list())
  beta_list = list()
  sigma_list = list()
  D_list = list()
  beta_inferred_list = list()
  rmse_sigma_list = list()
  rmse_D_list = list()
  m_list = sapply(1:K, function(z) vector())
  m_list_pre = list()
  ll_value_list = list()
  beta_inferred_list = sapply(1:N_EM_iteration, function(i) list())
  rmse_sigma_list = sapply(1:N_EM_iteration, function(i) list())
  rmse_D_list = sapply(1:N_EM_iteration, function(i) list())
  
  # Initial values for parameters
  for (k in 1:K) { 
    ## Random effect covariance matrix
    D_list[[k]] <- genPositiveDefMat("eigen", dim = q)$Sigma
    
    ## fixed effects
    beta_list[[k]] <- rtnorm(n = p, mean = 0, sd = 2, lower = 0, upper = 2)
    
    ## noise
    sigma_list[[k]] = runif(1)
  }
  
  ## Store initial values of the parameters
  D_list_init <- D_list
  beta_list_init <- beta_list
  sigma_list_init <- sigma_list
  
  ## Generate data
  for(i in 1:M) {
    Z[[i]] = cbind(1, 1:n_i)
    
    X[[i]] = matrix(rnorm(p*n_i),n_i,p)
  }
  
  cuts <- ceiling(M/K)
  for (k in 1:K) {
    if(k != K){
      ID_range <- c(1:M)[c(((cuts*(k-1))+1) : (cuts*k))]
    }else{
      ID_range <- c(1:M)[c(((cuts*(k-1))+1) : M)]
    }
    
    for(i in ID_range) {
      b[[i]] = matrix(rmvnorm(1, rep(0,q), D_list[[k]]), q, 1)
      y[[i]] = X[[i]] %*% beta_list[[k]] + Z[[i]] %*% b[[i]] + rnorm(n_i, 0, sigma_list[[k]])
    }
  }
 
  ## Responsibilities
  responsibilities_initial_iteration <- function(K){
    
    p_y <- list()
    p_y <- sapply(1:K, function(z) vector())
    
    for (k in 1:K) {
      p_y[[k]] <- runif(n=M, min=0, max=1)
    }
    
    for (k in 1:K) {
      m_list[[k]] <-  p_y[[k]] / Reduce('+', p_y)
    }
    return(m_list=m_list)
  }
  
  responsibilities <- function(M, m_list, y, X, beta_list, Z, D_list, sigma_list){
    
    p_y <- list()
    p_y <- sapply(1:K, function(z) vector())
    
    for (k in 1:K) {
      for (i in 1:M) {
        p_y[[k]][i] <- dmvnorm(c(y[[i]]), 
                               mean = X[[i]] %*% beta_list[[k]], 
                               sigma = (Z[[i]] %*% D_list[[k]] %*% t(Z[[i]])) + (sigma_list[[k]] * diag(n_i)))
      }
    }
    
    log_p_x <- list()
    log_p_x <- sapply(1:K, function(z) vector())
    
    for (k in 1:K) {
      for (i in 1:M) {
        log_p_x[[k]][i] <- 0
        for (t in 1:n_i) {
          log_p_x[[k]][i] <- log_p_x[[k]][i] + 
            log(dmvnorm(c(X[[i]][t,]), mean = mean_prime[[k]], sigma = Sigma_prime[[k]]))
        }
      }
    }
    
    px_zero_ind <- vector()
    for(k in 1:K){
      px_zero_ind <- append(px_zero_ind, which(lapply(log_p_x[[k]], exp)==0))
    }
    px_zero_ind <- unique(px_zero_ind)
    
    denominator_vec <- vector()
    for (i in 1:M) {
      denominator <- 0
      for (k in 1:K) {
        if(i %in% px_zero_ind){
          denominator <- 1
        }else{
          denominator <- denominator + (p_y[[k]][i] * exp(log_p_x[[k]][i]) * sum(m_list[[k]]))
          
          if(denominator==0){
            px_zero_ind <- sort(unique(append(px_zero_ind, i)))
            denominator <- 1
          }
        }
      }
      denominator_vec[i] <- denominator
    }
    
    m_list_temp <- list()
    m_list_temp <- sapply(1:K, function(z) vector())
    
    for (i in 1:M) {
      for (k in 1:K) {
        if(i %in% px_zero_ind){
          m_list_temp[[k]][i] <- 1/K
        }else{
          m_list_temp[[k]][i] <- 
            (p_y[[k]][i] * exp(log_p_x[[k]][i]) * sum(m_list[[k]])) / denominator_vec[i]
        }
      }
    }
    
    # denominator_vec <- vector()
    # for (i in 1:M) {
    #   denominator <- 0
    #   for (k in 1:K) {
    #     denominator <- denominator + (p_y[[k]][i] * sum(m_list[[k]]))
    #   }
    #   denominator_vec[i] <- denominator
    # }
    # 
    # m_list_temp <- list()
    # m_list_temp <- sapply(1:K, function(z) vector())
    # 
    # for (i in 1:M) {
    #   for (k in 1:K) {
    #     m_list_temp[[k]][i] <- 
    #       (p_y[[k]][i] *  sum(m_list[[k]])) / denominator_vec[i]
    #   }
    # }
    # 
    m_list <- m_list_temp
    return(m_list=m_list)
  }
  
  ## Initialization for optim
  init_vals = c(10,1,1)
  
  ## Objective needs to be negative log likelihood
  objective_fun <- function(param_vec, y, X, Z, N, m, pen_matrix) {
    -profile_log_likelihood(param_vec, y, 
                            X, Z, N, m, pen_matrix)$ll
  } 
  
  ## Spliting the sets to train and test 
  M_chunks <- list()
  M_chunks <- split(c(1:M), ceiling(seq_along(c(1:M))/N_folds))
  
  # Start CV
  cuts <- ceiling(M/N_folds)
  ID_test <- as.vector(unlist(split(c(1:M), ceiling(seq_along(c(1:M))/N_folds))[fold]))
  ID_train <- as.vector(unlist(split(c(1:M), ceiling(seq_along(c(1:M))/N_folds))[-fold]))
  
  X_train <- list(); X_test <- list()
  y_train <- list(); y_test <- list()
  Z_train <- list(); Z_test <- list()
  b_train <- list(); b_test <- list()
  
  X_train <- X[ID_train]; X_test <- X[ID_test]
  y_train <- y[ID_train]; y_test <- y[ID_test]
  Z_train <- Z[ID_train]; Z_test <- Z[ID_test]
  b_train <- b[ID_train]; b_test <- b[ID_test]
  
  N_train <- (length(X_train) * n_i)
  N_test <- (length(X_test) * n_i)
  M_train <- length(X_train)
  M_test <- length(X_test)
  
  pen_matrix_train <- sqrt(lambda/length(X_train))*Diagonal(p)
  
  ## EM algorithm
  for (iteration in 1:N_EM_iteration) {
    
    beta_inferred_list[[iteration]] <- list()
    rmse_sigma_list[[iteration]] <- list()
    rmse_D_list[[iteration]] <- list()
    
    if(iteration == 1){
      for (k in 1:K) {
        D_list[[k]] <- genPositiveDefMat("eigen", dim = q)$Sigma
        beta_list[[k]] <- rtnorm(n = p, mean = 0, sd = 2, lower = 0, upper = 2)
        sigma_list[[k]] = runif(1)
      }
    }
    
    ## E step (Update responsibilitis)
    if(iteration == 1){
      m_list <- responsibilities_initial_iteration(K)
      m_list_pre <- m_list
    }else{
      m_list <- responsibilities(M=length(X_train), m_list=m_list, y=y_train, X=X_train, beta_list=beta_list, Z=Z_train, D_list=D_list, sigma_list=sigma_list)
    }
    browser()
    ## Log_likelihood value of the previous iteration step
    if(iteration > 3) ll_value_last_three <- ll_value_last_two
    if(iteration > 2) ll_value_last_two <- ll_value_last
    if(iteration > 1) ll_value_last <- ll_value
    
    ll_value <- 0
    
    ## M step (estimate parameters (beta, sigma, D))
    ## Run optimization in parallel
    # Estimate mean and covariance parameters for X
    ########## updated   
    X_t_m <- sapply(1:K, function(z) list())
    mean_prime <- sapply(1:K, function(z) list(0))
    Sigma_prime <- list()
    
    for (k in 1:K) {
      m_vec <- vector()
      for(i in 1:M_train) {
        mean_prime[[k]] <- mean_prime[[k]] + as.vector(m_list[[k]][i] * colSums(X_train[[i]]))
        X_t_m[[k]][[i]] <- t(X_train[[i]]) * m_list[[k]][i]
        m_vec <- append(m_vec, rep(m_list[[k]][i], n_i))
      }
      mean_prime[[k]] <- mean_prime[[k]] / sum(m_list[[k]] * n_i)
      
      Sigma_prime[[k]] <- cov.wt(do.call(rbind, X_train), m_vec)$cov
      Sigma_prime[[k]] <- glasso(Sigma_prime[[k]], sqrt(2*M_train*log(p))/2, penalize.diagonal=FALSE)$w
    }
    
    optim_results <- list()
    # cores = detectCores()
    # cl <- makeCluster(cores[1])
    # registerDoParallel(cl)
    
    for (k in 1:K) {
      # optim_results_parallel <- foreach(k = 1:K) %dopar% {
      # library(expm)
      # library(Matrix)
      # library(matrixcalc)
      # library(here)
      # source(here('lmm_high_dim_optimization_more_than_one_group.R'))
      # clusterExport(cl, list("profile_log_likelihood",
      #                        "unconstrained_params_to_cov",
      #                        "expm"))
      
      optim_results[[k]] = optim(init_vals, objective_fun, y=y_train,
                                 X=X_train, Z=Z_train, N=N_train,
                                 m=m_list[[k]],
                                 pen_matrix=pen_matrix_train^2)
    }
    # stopCluster(cl)
    browser()
    for (k in 1:K) {
      
      ll_result <- profile_log_likelihood(optim_results[[k]]$par, y_train,
                                          X_train, Z_train, N_train,
                                          m=m_list[[k]], pen_matrix_train)
      
      R_all <- as.matrix(ll_result$R_all)
      
      ## Recover parameters
      beta_inferred <- solve(R_all[1:p,1:p])%*%R_all[1:p,p+1]
      sigma2_inferred <- R_all[(p+1):dim(R_all)[1],dim(R_all)[2]]^2/N_train
      D_sigma <- unconstrained_params_to_cov(optim_results[[k]]$par, q)
      ll_value <- ll_value + as.numeric(ll_result$ll)
      
      ## Plot
      # print(D_sigma*sigma2_inferred) # D recovered?
      # print(sqrt(sigma2_inferred)) # Sigma recovered?
      
      ## Save results
      beta_inferred_list[[iteration]][k] <- list(beta_inferred)
      rmse_sigma_list[[iteration]][k] <- list(rmse(sqrt(sigma2_inferred), sigma_list[[k]]))
      rmse_D_list[[iteration]][k] <- list(rmse((D_sigma*sigma2_inferred), D_list[[k]]))
      
      ## Update parameters
      sigma_list[[k]] <- sqrt(sigma2_inferred)
      beta_list[[k]] <- beta_inferred
      D_list[[k]] <- D_sigma*sigma2_inferred
      
      ## Size of the groups
      size[[iteration]][[k]] <- list(sum(m_list[[k]] >= 1/K))
      
    }
    
    ## Detect the true group labels and assign beta
    for (k_fix in 1:K) {
      beta_rmse_labeling <- vector()
      for (k in 1:K) {
        beta_rmse_labeling <- append(beta_rmse_labeling,
                                     rmse(beta_inferred_list[[iteration]][[k_fix]],
                                          beta_list_init[[k]]))
      }
      true_label <- which.min(beta_rmse_labeling)
      plot(beta_list_init[[true_label]],
           as.vector(beta_inferred_list[[iteration]][[k_fix]]))
    }
    
    ## Save results of log-likelihood for different iteration
    ll_value_list[[iteration]] <- ll_value
    
    cat("lambda = ", lambda, '\n')
    cat("fold = ", fold, '\n')
    cat("log_likelihood =           ", as.numeric(ll_value), '\n')
    
    ## Stop EM if log_likelihood has almost converged
    if(iteration > 5){
      if(abs(ll_value - ll_value_last) < 1) break()
    }
    
    ## Stop EM if log_likelihood convergence has trapped in a loop
    if(iteration > 4){
      if(round(ll_value, 1) == round(ll_value_last_two, 1) &&
         round(ll_value_last, 1) == round(ll_value_last_three, 1)) break()
    }
    
    ## Stop EM if size of one of the groups is small
    stop_loop = FALSE
    if(iteration > 1){
      for (k in 1:K) {
        if(sum(m_list[[k]] > 0.1) <= (length(X_train)/(10*K))) {
          stop_loop = TRUE
          break()
        }
        if(stop_loop) break()
      }
      if(stop_loop) break()
    }
    if(stop_loop) break
  }
  
  ## Penalization matrix for the held-out test set
  pen_matrix_test <- sqrt(lambda/length(X_test))*Diagonal(p)
  
  ## For a given fold and a given lambda calculate the log-likelihood values
  ## on the test set for each group and put them in a vector
  ll_result_test_K <- vector()
  for(k in 1:K){
    ll_result_test_K <-
      append(ll_result_test_K,
             as.numeric(profile_log_likelihood(optim_results[[k]]$par, y_test,
                                               X_test, Z_test, N_test,
                                               m=m_list[[k]], pen_matrix_test)$ll))
  }
  
  browser()
  # Save the result 
  if(t == 1){
    return(list('ll_result_test_K'=ll_result_test_K,
                'N_folds'=N_folds,
                'fold'=fold,
                'lambda'=lambda,
                'lambdas'=lambdas,
                't'=t,
                'M'=M, 
                'K'=K, 
                'N_EM_iteration'=N_EM_iteration, 
                'n_i'=n_i, 
                'N'=N, 
                'p'=p, 
                'q'=q,
                'y'=y, 
                'X'=X, 
                'Z'=Z, 
                'b'=b, 
                'D_list_init'=D_list_init, 
                'beta_list_init'=beta_list_init, 
                'sigma_list_init'=sigma_list_init))
  }else{
    return(list('ll_result_test_K'=ll_result_test_K,
                'fold'=fold,
                'lambda'=lambda,
                'lambdas'=lambdas,
                'K'=K,
                't'=t,
                'p'=p,
                'M'=M))
  }
}

ll_result_test_K <- ljcr_cv(M = 100,
                            n_i = 10,
                            K = K,
                            p = 100,
                            N_folds = N_folds,
                            N_lambdas = N_lambdas,
                            N_EM_iteration = 40)

# saveRDS(ll_result_test_K, file.path(storage, paste0("ll_result_test_M_",ll_result_test_K$M,
                                                    "_p_",ll_result_test_K$p,"_K_",K,
                                                    "_fold_",ll_result_test_K$fold,
                                                    "_lambda_",which(ll_result_test_K$lambdas == ll_result_test_K$lambda),".RData")))
