# Instructions
These instructions are intended to explain how to reproduce the figures in the paper "A penalized longitudinal mixed model withlatent group structure, with an application inneurodegenerative diseases" (Hatami et al., 2022). All code is in R unless otherwise specified.
There is one application (on ALS data) and different simulation studies described in the paper.

## Simulation study
All the data and results are available in this repository (use git lfs) to retrieve the files via git lfs pull.

### Generating the simulation data
To re-generate the data from scratch first of all make sure all the scripts and functions in 'Code', 'Data' and 'Visualization' are stored in one place.
To generate the simulation data, please run the following script:
```
generate_sim_data.R
```
Note that this will overwrite any existing simulation data in your local repository unless you change the file names. The above script calls  the following source file which involves applying the LJCR model on the simulated data:
```
ljcr_on_sim_data.R
```

## ALS Aplication
To reproduce the application to the PROACT ALS data, you will first need to download the data, as we do not have permission to redistribute it. The ALS data can be found [here](https://nctu.partners.org/ProACT). You will need the following files:

```
all_forms_PROACT.txt
ALSFRS_slope_PROACT.txt
```

### Pre-processing
The preprocessing for the ALS data is completely involved in a single script. Please use the following script which takes care of pre-processing:
```
als_processing.R
```
You can then apply the LJCR model on the resulted ALS table running the following script:
```
ljcr_on_als.R
```
The above mentioned model script will call the regularization part of the modeling by sourcing 'regularization_func.R'.

## Generating Figures
All figures in the paper can be reproduced by executing the script in Visualization/figures_paper.R. Please remember that you should place all files in both folders 'Data' and 'Visualization' in a common folder and the call the script from there. The script will generate a single plot for each of the figures including both for the simulation part and the ALS application.
